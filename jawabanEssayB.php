<?php
    function perolehan_medali($md_array){
        if( !$md_array ){
            return "no data";
        }
        $result = [];
        foreach( $md_array as $input ){
            $negara = $input[0];
            $medali = $input[1];

            $country = [];
            $found = false;
            $tempResult = [];
            foreach( $result as $info ){
                if( $info['negara'] == $negara ){
                    $country = $info;
                    $found = true;
                } else {
                    $tempResult[] = $info;
                }
            }
            if( !$found ){
                $country['negara'] = $negara;
                $country['emas'] = 0;
                $country['perak'] = 0;
                $country['perunggu'] = 0;
            }

            $country[$medali] += 1;
            $tempResult[] = $country;
            $result = $tempResult;
        }
        return $result;
    }
    $output = perolehan_medali(
        array(
            array("Indonesia", "emas"),
            array("India", "perak"),
            array("Korea Selatan", "emas"),
            array("India", "perak"),
            array("India", "emas"),
            array("Indonesia", "perak"),
            array("Indonesia", "emas")
        )
    );
    foreach($output as $o ){
        print_r ($o);
        echo "<br>";
    }
?>